trigger VEEVA_BEFORE_INVENTORY_INSERT on Inventory_Monitoring_vod__c (before insert) {
for (Integer i = 0; i <Trigger.new.size(); i++) {
        Inventory_Monitoring_vod__c  recNew = Trigger.new[i];
        if (recNew.Status_vod__c == 'Submitted_vod') {
            recNew.Lock_vod__c = true;    
        }
                
        if (recNew.Entity_Reference_Id_vod__c != null && 
            recNew.Entity_Reference_Id_vod__c.length() > 0) {
            
            recNew.Account_vod__c = recNew.Entity_Reference_Id_vod__c;
            recNew.Entity_Reference_Id_vod__c = null;
                
        }
    }
}