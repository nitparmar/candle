trigger VEEVA_BEFORE_INVENTORY_UPDATE on Inventory_Monitoring_vod__c (before update) {
	
    boolean modAllData = VOD_Utils.canModifyAllData();

    for (Integer i = 0; i <Trigger.new.size(); i++) {
        Inventory_Monitoring_vod__c recNew = Trigger.new[i];
 
        Inventory_Monitoring_vod__c recOld = Trigger.old[i];
        
        if (recNew.Lock_vod__c == true && modAllData != true) {
           recNew.Id.addError(System.Label.NO_MODIFY_INVENTORY_MONITORING, false);
        }
        
        if (recOld.Lock_vod__c == true && recNew.Lock_vod__c == false) {
           recNew.Status_vod__c = 'Saved_vod';
        }
        
        if (recOld.Status_vod__c != 'Submitted_vod' &&recNew.Status_vod__c == 'Submitted_vod') {
           recNew.Lock_vod__c = true;    
        }    
    }        
}