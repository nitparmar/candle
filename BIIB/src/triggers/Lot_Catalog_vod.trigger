trigger Lot_Catalog_vod on Lot_Catalog_vod__c (before insert, before update) {
    String label  = System.label.USE_MULTI_SAMPLE_vod;             
    Boolean bUseSamp = false;  
    if (label != null && label != 'false') {
        bUseSamp = true;
    } 
    For (Lot_Catalog_vod__c lot : Trigger.new) {
       if (bUseSamp == true) { 
           lot.Lot_Catalog_External_Id_vod__c = lot.Sample_vod__c.replaceAll(' ', '_') + '_' + lot.Name; 
       } 
       else 
        { 
            lot.Lot_Catalog_External_Id_vod__c = lot.Name;
        }  
    }                
}