trigger BIIB_AccountTrigger on Account(before insert, before update, after insert, after update) 
{
    //Before Insert 
    if(Trigger.isInsert && Trigger.isBefore){
    
    }
    //Before Update
    if(Trigger.isUpdate && Trigger.isBefore){
    
    }
    //After Insert
    if(Trigger.isInsert && Trigger.isAfter){
        BIIB_AccountTrigger_Handler.OnAfterInsert(Trigger.new);   
    }
    //After Update
    if(Trigger.isUpdate && Trigger.isAfter){
        BIIB_AccountTrigger_Handler.OnAfterUpdate(Trigger.new,Trigger.oldMap);   
    }    
}