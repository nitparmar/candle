<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Call Objective can be accessed through Call Report</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>Account_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The account that this tasks belongs to</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Call Objectives</relationshipLabel>
        <relationshipName>Call_Objectives_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Event_Target_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Business Event Target</description>
        <externalId>false</externalId>
        <label>Business Event Target</label>
        <referenceTo>Business_Event_Target_vod__c</referenceTo>
        <relationshipLabel>Call Objectives</relationshipLabel>
        <relationshipName>Call_Objectives_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Event_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>the event this task belongs to</description>
        <externalId>false</externalId>
        <label>Business Event</label>
        <referenceTo>Business_Event_vod__c</referenceTo>
        <relationshipLabel>Call Objectives</relationshipLabel>
        <relationshipName>Call_Objectives_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Call2_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>the call that this task belongs to</description>
        <externalId>false</externalId>
        <label>Call</label>
        <referenceTo>Call2_vod__c</referenceTo>
        <relationshipLabel>Call Objectives</relationshipLabel>
        <relationshipName>Call_Objectives_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Comment_vod__c</fullName>
        <description>The comment for this task</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Completed_Flag_vod__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If MR finish this task, this flag is true</description>
        <externalId>false</externalId>
        <label>Completed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Date_vod__c</fullName>
        <description>The date time of plan or activity date for task</description>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Delivery_Method_vod__c</fullName>
        <description>The way of EPPV or PI</description>
        <externalId>false</externalId>
        <label>Delivery Method</label>
        <picklist>
            <picklistValues>
                <fullName>Call</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FAX</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DM</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NCW_vod</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>From_Date_vod__c</fullName>
        <description>The date range for each actions</description>
        <externalId>false</externalId>
        <label>From</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Information_vod__c</fullName>
        <description>Information note for this task</description>
        <externalId>false</externalId>
        <label>Information</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NCW_Reason_vod__c</fullName>
        <description>The reason of why does the MR could not complied with this task</description>
        <externalId>false</externalId>
        <label>NCW Reason</label>
        <picklist>
            <controllingField>Delivery_Method_vod__c</controllingField>
            <picklistValues>
                <fullName>Pass this MS</fullName>
                <controllingFieldValues>NCW_vod</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Recall</fullName>
                <controllingFieldValues>NCW_vod</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No time</fullName>
                <controllingFieldValues>NCW_vod</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <controllingFieldValues>NCW_vod</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Name_vod__c</fullName>
        <externalId>false</externalId>
        <label>Name</label>
        <length>80</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_Of_PIs_vod__c</fullName>
        <description>The number of PI copy which the MR pass</description>
        <externalId>false</externalId>
        <label>Number Of PIs</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_Of_Patients_vod__c</fullName>
        <description>The number of patients to which the doctor prescribes the new drug</description>
        <externalId>false</externalId>
        <label>Number Of Patients</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Pre_Explain_Flag_vod__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If the task is pre-explain call, this flag is true</description>
        <externalId>false</externalId>
        <label>Pre-Explain</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to the Product Catalog for this objective.</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product_vod__c</referenceTo>
        <relationshipLabel>Call Objectives</relationshipLabel>
        <relationshipName>Call_Objectives_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>To_Date_vod__c</fullName>
        <description>The date range for each actions</description>
        <externalId>false</externalId>
        <label>To</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Call Objective</label>
    <nameField>
        <displayFormat>CO-{00000000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Call Objectives</pluralLabel>
    <recordTypes>
        <fullName>EPPV_vod</fullName>
        <active>true</active>
        <description>EPPV</description>
        <label>EPPV</label>
        <picklistValues>
            <picklist>Delivery_Method_vod__c</picklist>
            <values>
                <fullName>Call</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DM</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>FAX</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NCW_vod</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>NCW_Reason_vod__c</picklist>
            <values>
                <fullName>No time</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pass this MS</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Recall</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>PI_vod</fullName>
        <active>true</active>
        <description>PI</description>
        <label>PI</label>
        <picklistValues>
            <picklist>Delivery_Method_vod__c</picklist>
            <values>
                <fullName>Call</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DM</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>FAX</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NCW_vod</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>NCW_Reason_vod__c</picklist>
            <values>
                <fullName>No time</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pass this MS</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Recall</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
