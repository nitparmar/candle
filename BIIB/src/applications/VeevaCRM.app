<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Veeva CRM from Veeva Systems</description>
    <label>Veeva CRM</label>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>Product_vod__c</tab>
    <tab>MySetup</tab>
    <tab>Account_Plan_vod__c</tab>
    <tab>MyAccounts</tab>
    <tab>MySchedule</tab>
    <tab>Time_Off_Territory_vod__c</tab>
    <tab>Product_Plan_vod__c</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>Call2_Expense_vod__c</tab>
    <tab>Call2_Sample_vod__c</tab>
    <tab>Clinical_Trial__c</tab>
    <tab>Publication__c</tab>
    <tab>Speaker_Evaluation__c</tab>
    <tab>Material_Order__c</tab>
    <tab>Benefit_Design_vod__c</tab>
    <tab>Benefit_Design_Line_vod__c</tab>
    <tab>MySamples</tab>
    <tab>Sample_Transaction_vod__c</tab>
    <tab>Sample_Inventory_vod__c</tab>
    <tab>Medical_Event_vod__c</tab>
    <tab>Analytics</tab>
    <tab>VMobile_Object_Configuration_vod__c</tab>
    <tab>Sample_Order_Transaction_vod__c</tab>
    <tab>Coaching_Report_vod__c</tab>
    <tab>Alert_vod__c</tab>
    <tab>Sample_Limit_vod__c</tab>
    <tab>Product_Order_Allocation_vod</tab>
    <tab>Call2_vod__c</tab>
    <tab>Business_Event_vod__c</tab>
    <tab>BIIB_Deployment_Tracking__c</tab>
    <tab>BIIB_FACProgram__c</tab>
    <tab>TSF_vod__c</tab>
    <tab>BIIB_Literature__c</tab>
</CustomApplication>
