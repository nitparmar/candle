@isTest(SeeAllData=true)
public with sharing class BIIB_Test_Escalate_To_Field 
{
    static testMethod void  BIIB_Test_Escalate_To_Field()
    {
        // Create Test Data For Work Item-Record Type-Work Item 1
        
        //Creating Account Test data
        RecordType objAccRecordType = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND DeveloperName='BIIB_Patient' limit 1];
        System.debug('***objAccRecordType*****' +objAccRecordType.Id);
        Account objAccount = new Account(FirstName = 'Steve' , LastName = 'Denver' , RecordTypeId = objAccRecordType.Id , PersonDoNotCall = false);                                         
        insert  objAccount;
        system.debug('***objAccount*****' +objAccount.Id);
        
        //Creating Case Test data
        RecordType objCaseRecordType = [SELECT Id,Name FROM RecordType WHERE SObjectType='Case' AND DeveloperName='BIIB_Benefit_Investigation' limit 1];
        System.debug('***objCaseRecordType*****' +objCaseRecordType.Id);
        Case objCase = new Case(BIIB_Is_Pre_Created__c = true, BIIB_Comments__c = 'testcase', AccountId = objAccount.Id,RecordTypeId = objCaseRecordType.Id,Status = 'New');                                        
        insert  objCase;
        system.debug('***objCase*****' +objCase.Id);
        
        RecordType objAccWIType = [SELECT Id,Name FROM RecordType WHERE SObjectType='BIIB_Work__c' AND DeveloperName='BIIB_Work_Item_1' limit 1];
         //Creating Work Item Test data
         BIIB_Work__c objWork = new BIIB_Work__c(Name = 'A001',
                                        BIIB_Account__c = objAccount.Id,
                                        BIIB_Assigned_to__c = '00511000001Hi3r',
                                        BIIB_ININ_ID__c = '666666',
                                        BIIB_Preferred_Language__c = 'English',
                                        BIIB_Preferred_Time__c = '8PM',
                                        BIIB_Priority__c = '85',
                                        BIIB_Region__c = 'UCAN',
                                        BIIB_Status__c = 'Closed-Failed',
                                        BIIB_Work_Group__c = 'Acquisition',
                                        RecordTypeId = objAccWIType.Id,
                                        BIIB_Case__c = objCase.Id
                                        
                                        );
                                        
            insert  objWork;                    
            system.debug('---objWork----'+objWork);
            system.debug('---objWorkId----'+objWork.Id);
            BIIB_Work__c objWorkQuery=[SELECT Id, Name from BIIB_Work__c where Name = 'A001'];
            system.debug('---objWorkQuery----'+objWorkQuery.Id);
            System.assertEquals(objWorkQuery.Id, objWork.Id);
            
            
            // Instantiate Actual Class
            
            BIIB_Escalate_To_Field_Class.BIIB_Escalate_To_Field(objWork.Id);
            System.assertEquals('CreateAdditionalWorkItem', 'CreateAdditionalWorkItem');
            
            
            
        // Create Test Data For Work Item-Record Type-Work Item 2 which cannot be escalated
        
        //Creating Account Test data
        RecordType objAccRT = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND DeveloperName='BIIB_Patient' limit 1];
        System.debug('***objAccRecordType*****' +objAccRT.Id);
        Account objAccount2 = new Account(FirstName = 'Mark' , LastName = 'Boucher' , RecordTypeId = objAccRT.Id , PersonDoNotCall = false);                                        
        insert  objAccount2;
        system.debug('***objAccount*****' +objAccount2.Id);
        
        //Creating Case Test data
        RecordType objCaseRecordType2 = [SELECT Id,Name FROM RecordType WHERE SObjectType='Case' AND DeveloperName='BIIB_Benefit_Investigation' limit 1];
        System.debug('***objCaseRecordType*****' +objCaseRecordType2.Id);
        Case objCase2 = new Case(BIIB_Is_Pre_Created__c = true, BIIB_Comments__c = 'testcase2', AccountId = objAccount2.Id,RecordTypeId = objCaseRecordType2.Id,Status = 'New');                                        
        insert  objCase2;
        system.debug('***objCase*****' +objCase2.Id);
        
        RecordType objWIType = [SELECT Id,Name FROM RecordType WHERE SObjectType='BIIB_Work__c' AND DeveloperName='BIIB_Work_Item_2' limit 1];
         //Creating Work Item Test data
         BIIB_Work__c objWork2 = new BIIB_Work__c(Name = 'A002',
                                        BIIB_Account__c = objAccount2.Id,
                                        BIIB_Assigned_to__c = '00511000001Hi3r',
                                        BIIB_ININ_ID__c = '666666',
                                        BIIB_Preferred_Language__c = 'English',
                                        BIIB_Preferred_Time__c = '8PM',
                                        BIIB_Priority__c = '85',
                                        BIIB_Region__c = 'UCAN',
                                        BIIB_Status__c = 'Closed-Failed',
                                        BIIB_Work_Group__c = 'REMS',
                                        RecordTypeId = objWIType.Id,
                                        BIIB_Case__c = objCase2.Id
                                        
                                        );
                                        
            insert  objWork2;                   
            system.debug('---objWork2----'+objWork2);
            system.debug('---objWorkId----'+objWork2.Id);
            BIIB_Work__c objWorkQuery2=[SELECT Id, Name from BIIB_Work__c where Name = 'A002'];
            system.debug('---objWorkQuery----'+objWorkQuery2.Id);
            System.assertEquals(objWorkQuery2.Id, objWork2.Id);
            
            
            // Instantiate Actual Class
            
            BIIB_Escalate_To_Field_Class.BIIB_Escalate_To_Field(objWork2.Id);
            System.assertEquals('NoEscalate', 'NoEscalate');
            
    }
}