/**
*@author Aditya Ghosh
*@date 25/04/2014
@test class for BIIB_LastModifiedController
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Aditya       22/04/2014  OriginalVersion
*/
@isTest(seeAllData = True)
public class BIIB_LastModifiedController_Test{

    public static testmethod void testFunction(){
        //Account tempAcc = BIIB_testClassHelper.createAccountPatient('testAcc','testAcclast');
        //insert tempAcc;
        Account tempAcc = [Select id,LastModifiedDate from Account limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(tempAcc);
        BIIB_LastModifiedController testInstance = new BIIB_LastModifiedController(sc);
        testInstance.checkPopupShow();
        testInstance.updateRec();
     }
}