/**
*@author Kamal Mehta
*@date 07/04/2014
@description controller class for BIIB_Benefit_Investigation page
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Kamal       07/04/2014  OriginalVersion
*/

public with sharing class BIIB_BIExtensionClass {

    public List<BIIB_Patient_Plan__c>BIList{get;set;}
    public List<BIIB_Patient_Plan__c>localBIList{get;set;}
    public String strBIId {get;set;}
    public boolean biTableRendered {get;set;}
    public String patientId = '';
    public String patientName = '';
    public Id recTypeId=null;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPageNumber;
    
    //constructor
    public BIIB_BIExtensionClass(ApexPages.StandardController controller)
     {
        recTypeId = BIIB_Utility_Class.getRecordTypeId('BIIB_Patient_Plan__c','Benefit Investigation');
        Case Owork =(Case)controller.getRecord();
        biTableRendered = false;
        List<Case> cList = new List<Case>();
        cList = [select Id, AccountId,Account.Name from Case where Id=:Owork.Id limit 1];
        if(!(cList.isEmpty())){
            patientId = cList[0].AccountId;
            patientName=cList[0].Account.Name;
            patientName=patientName.replace(' ', '+');
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Service Request is not linked to valid patient ');
            ApexPages.addMessage(myMsg);
            
        }
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = Integer.valueOf(Label.BIIB_Page_Size_Infusion_Search);
        localBIList = [Select Id, Name, BIIB_Patient_Lookup__c, BIIB_Plan__c, BIIB_Effective_Date__c, BIIB_End_Date__c from BIIB_Patient_Plan__c where BIIB_Patient_Lookup__c = :patientId and recordtype.Id=:recTypeId];

        if(localBILIst.isEmpty()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Patient is not linked to any Plan. Please click on the new button to associate a plan ');
            ApexPages.addMessage(myMsg);            
        }
        system.debug('*'+ApexPages.currentPage().getParameters().get('Id'));
        BindData(1);
     }
     /*
    * @author Kamal Mehta
    @description This method is used for pagination
    @param Integer newPageIndex
    @return void
    */       
    public void BindData(Integer newPageIndex)
    {
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;

            }
            
            pageNumber = newPageIndex;
            BIList = new List<BIIB_Patient_Plan__c>();
            for(Integer i=min; i < max ; i++)
            {
                if (localBIList.size() > i)
                    BIList.add(localBIList[i]);
            }
            
            if(!BIList.isEmpty() )
            {
                biTableRendered=true;
            }else{
                biTableRendered=false;
            }
    }
    /*
    * @author Kamal Mehta
    @description This method is used to change the page reference to the edit page
    @param none
    @return PageReference
    */  
    public PageReference editBI(){
        PageReference p = new PageReference('/'+strBIId+'/e?retURL=%2Fa'+strBIId);
        return p;
    }
    /*
    * @author Kamal Mehta
    @description This method is used to change the page reference when a new Benefit Investigation is to be made.
    @param none
    @return PageReference
    */  
    public PageReference newBI(){
        
        if(recTypeId != null) {
           String keyprefixPP = Schema.getGlobalDescribe().get('BIIB_Patient_Plan__c').getDescribe().getKeyPrefix();
            BIIB_Object_FieldId__c patientField= BIIB_Object_FieldId__c.getInstance('BIIB_Patient_Plan__c_Patient__c');
            PageReference p = new PageReference('/'+keyprefixPP +'/e?retURL=%2F'+keyprefixPP+'%2Fo&RecordType='+recTypeId+'&'+patientField.BIIB_Field_Id__c+'='+patientName+'&'+ patientField.BIIB_Field_Id__c+'_lkid='+patientId);
            return p;
         }else{
            return null;
         }  
    }   
    
    /*
    * @author Kamal Mehta
    @description Returns the current page number to the page.
    @param none
    @return Integer(page number)
    */
    public Integer getPageNumber()
    {
        return pageNumber;
    }
    
    /*
    * @author Kamal Mehta
    @description Returns the current page size to the page.
    @param none
    @return Integer(page number)
    */
    public Integer getPageSize()
    {
        return pageSize;
    }
    
    /*
    * @author Kamal Mehta
    @description Returns the boolean whether to enable the previous button or not.
    @param none
    @return Boolean
    */
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    /*
    * @author Kamal Mehta
    @description Returns the boolean whether to enable the next button or not.
    @param none
    @return Boolean
    */
    public Boolean getNextButtonDisabled()
    {
        if (localBIList == null) return true;
        else
        return ((pageNumber * pageSize) >= localBIList.size());
    }
    
    /*
    * @author Kamal Mehta
    @description Returns the total page number based on the no of records and page size.
    @param none
    @return Integer
    */
    public Integer getTotalPageNumber()
    {

        totalPageNumber = localBIList.size() / pageSize;
        Integer mod = localBIList.size() - (totalPageNumber * pageSize);
        if (mod > 0)
            totalPageNumber++;
        return totalPageNumber;
    }
    
    /*
    * @author Kamal Mehta
    @description Returns the total page number based on the no of records and page size.
    @param none
    @return Integer
    */
    public PageReference nextBtnClick()
     {
        BindData(pageNumber + 1);
        return null;
    }
    
    /*
    * @author Kamal Mehta
    @description Returns next page number to the page.
    @param none
    @return Integer
    */
    public PageReference previousBtnClick()
     {
        BindData(pageNumber - 1);
        return null;
    }

}