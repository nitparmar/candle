/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/21/2014
 * Author : Pruthvi Raj
 * Description : This Class is used for testing the functionality of  BIIB_SPP_PayerMatrix .
 * 
 */
@isTest
public class BIIB_SPP_PayerMatrixTest {

    public static testMethod void myUnitTest() {
      
         User tempUser = [Select id from User limit 1];
         
         RecordType acc_rectype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND DeveloperName  = 'BIIB_Patient'];
        Account acc_pat_rec = new Account(FirstName = 'Test' , LastName = 'Name' , RecordTypeId = acc_rectype.id , PersonDoNotCall = true);
        insert acc_pat_rec ;
          
        Case tempCase = BIIB_testClassHelper.createCase();
        insert tempCase;
        
        BIIB_Work__c tempWork = BIIB_testClassHelper.createWork(tempUser , tempCase );
        insert tempWork;
        tempwork.BIIB_Account__c = acc_pat_rec.id;
        update tempWork;
        BIIB_Work__c tempWork_ret = [SELECT Id,Name , BIIB_AccountRecordType__c , BIIB_Account__c , BIIB_Assigned_to__c , BIIB_Status__c , BIIB_Preferred_Language__c , BIIB_Case__c FROM BIIB_Work__c where Id=:tempWork.id];
        
        BIIB_PreferredSPPPlan__c pp = new BIIB_PreferredSPPPlan__c();
        insert pp;
        
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Record',tempWork_ret.id);
        BIIB_SPP_PayerMatrix obj = new BIIB_SPP_PayerMatrix(new ApexPages.StandardController(tempWork_ret));     
        obj.save(); 
        List<SelectOption> myOptions = new List<SelectOption>();
        myOptions = obj.getItems();
        Test.stopTest();
    }
    
}