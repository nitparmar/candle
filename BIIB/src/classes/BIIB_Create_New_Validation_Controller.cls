public with sharing class BIIB_Create_New_Validation_Controller {

    public BIIB_SOQLQueryFilterResult FilterResult {get; set;}
    public SObject Object2Filter {get; set;}
    
    public String strValidationName {get;set;}
    public String strValidationDescription {get;set;}
    public String strValidationMessage{get;set;}
    public Boolean boolValidation{get;set;}

    public BIIB_Create_New_Validation_Controller () {

		// be sure to initialize your SOQLQueryFilterResult and
        // default WhereConditionString in your page's constructor
		boolValidation = true;
		FilterResult = new BIIB_SOQLQueryFilterResult();
        FilterResult.WhereConditionString = '';
		
		
				
		
        Account objAccount = new Account(); 
        Object2Filter = objAccount;
     }
}