/**
*@author Kamal Mehta
*@date 14/04/2014
@description controller class for edit multiple work items.
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Kamal       14/04/2014  OriginalVersion
*/
public class BIIB_EditMultipleWorkItem
{

    Public List<BIIB_Work__c> lstWork {get;set;}
    private Case objCase = new Case();
    
    public BIIB_EditMultipleWorkItem(ApexPages.StandardController ctrl)
    {
        objCase  = (Case)ctrl.getRecord();
        if(objCase.Id != null)
        {
            lstWork  = [SELECT Name, BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c FROM BIIB_Work__c WHERE BIIB_Case__c=:objCase.Id];
        }
    }
     /*
    * @author Kamal Mehta
    @description This methods updates all the work items at one go.
    @param null
    @return pagereference
    */
    
    public pagereference Save()
    {
        try
        {
            update lstWork;
            PageReference pgRef = new PageReference('/'+objCase.Id);
            pgRef.setRedirect(true);
            return pgRef;
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
}