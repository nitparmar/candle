global with sharing class VEEVA_ApprovedDocumentAccessChecker{
        
    Global Map<Id, Boolean> userHasAccessToApprovedDocuments(Set<Id> documentIds) {
        Map<Id, Boolean> result = new Map<Id, Boolean>();
        Map<Id, Approved_Document_vod__c> docMap = new Map<Id, Approved_Document_vod__c>([select Id, Name from Approved_Document_vod__c where id IN :documentIds]);
        for(Id docId: documentIds){
            result.put(docId, docMap.containsKey(docId));            
        }
        return result;
    }


}