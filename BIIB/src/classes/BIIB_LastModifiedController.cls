/**
*@author Rashi Joshi
*@date 23/04/2014
@description Controller class for BIIB_Last_Modified and BIIB_OpenPopup Pages to Open a Pop-up when Patient Profile is out of date.
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Rashi       23/04/2014  OriginalVersion
*/
public class BIIB_LastModifiedController{
    public boolean displayPopup {get; set;} 
    public Account objAccount {get; set; }
    
    public BIIB_LastModifiedController(ApexPages.StandardController controller)
    {
        objAccount = (Account)controller.getRecord();
        system.debug('----------objAccount------------'+objAccount.Id);
        displayPopup  = false;
     }
     /*
    * @author Rashi Joshi
    @description This method is used to check if the Popup needs to be shown.
    @param none
    @return void
    */ 
    public void checkPopupShow()
    {
        system.debug('-------------checkpopupshow-------------');
        Date varLastModifiedDate = objAccount.LastModifiedDate.Date();
        system.debug('----------objAccount.LastModifiedDate------------'+objAccount.LastModifiedDate);
        //Checking if days between Last Modified date and today exceeds 180 days (value in Label BIIB_LastModifiedDays)
        if(varLastModifiedDate.DaysBetween(System.Today()) > Integer.valueof(Label.BIIB_LastModifiedDays))
        {
           displayPopup  = true; 
        }
    }
    
    /*
    * @author Rashi Joshi
    @description This method is used to redirect the user to edit page if the user clicks 'No'
    @param none
    @return void
    */
    
    public void updateRec()
    {
       // objAccount.name = objAccount.name;
        objAccount.Id = objAccount.Id;
        update objAccount;
    }
    
}