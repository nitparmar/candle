/*  @Name: BIIB_Escalate_To_Field_Class
    @Author: Divya Galani 
    @Date:16-Apr-14
    @Description:US-0086
       1. This is a global class and is invoked from the button "Escalate To Field" on Work Custom Object
       2. It validates 3 conditions: 
            If a particular Work Item Record Type can be escalataed or not
            If the Maxmimum No Of Attempts for a particular Work Item Record have been reached or not
            If an "Escalate To Field" work item can be created for the Current incoming Work Item
        3. The result is returned back to the button functionality
       Modification Log
       -----------------------------------------------------------------------------------------------
       Developer                Date                Description
       Divya Galani             16-Apr-14           Original Version
       -----------------------------------------------------------------------------------------------
       
*/

global class BIIB_Escalate_To_Field_Class 
{
    webService static String BIIB_Escalate_To_Field(String strWorkItemId) 
    {
            String strOutput = '';
            String strRecordType;
            String strCaseId;
            Integer intNoOfAttempts;
            String strWorkItemRecordType;
            List<BIIB_Work__c> lstWorkItem = new List<BIIB_Work__c>();
            List<BIIB_Work__c> lstWorkRecordType = new List<BIIB_Work__c>();

    
            //Get RecordType of the current Work Item Record
            lstWorkRecordType = Database.query('SELECT RecordType.Name FROM BIIB_Work__c WHERE Id =: strWorkItemId');
            if(lstWorkRecordType.size() > 0)
            {
                for(BIIB_Work__c objWorkRecordType :lstWorkRecordType)
                {
                    strWorkItemRecordType = objWorkRecordType.RecordType.Name;
                }
            }// End if(lstWorkRecordType.size() > 0)
            
            // Query RecordType details for the incoming record
            for(RecordType rt : [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType = 'BIIB_Work__c' AND Name =:strWorkItemRecordType])
            {
                strRecordType = rt.DeveloperName;
                system.debug('--------strRecordType------'+strRecordType);
            }
            //Query Custom Setting to fetch the "No Of Attempts" field for all Work Item Record Types
            List<BIIB_Work_Item_RecordType__c> lstWorkItemRecordType = [SELECT BIIB_No_Of_Attempts__c,BIIB_Can_Be_Escalated__c FROM BIIB_Work_Item_RecordType__c WHERE Name =:strRecordType];
            
            // Validate if this Work Item Record Type can be escalated or not
            for(BIIB_Work_Item_RecordType__c objWorkItemRecordType : lstWorkItemRecordType)
            {
                intNoOfAttempts  = Integer.valueOf(objWorkItemRecordType.BIIB_No_Of_Attempts__c);
                Boolean CanEscalate = objWorkItemRecordType.BIIB_Can_Be_Escalated__c;
                
                if(objWorkItemRecordType.BIIB_Can_Be_Escalated__c == false)
                {
                    strOutput = 'NoEscalate';
                    system.debug('--------strOutputNoEscalate------'+strOutput);
                }
            }// End for(BIIB_Work_Item_RecordType__c objWorkItemRecordType : lstWorkItemRecordType)
            
            // Get Case Id for the Current Work Item Record
            lstWorkItem = Database.query('SELECT Id,BIIB_Case__c FROM BIIB_Work__c WHERE Id =:strWorkItemId');
            strCaseId = lstWorkItem[0].BIIB_Case__c;
            
            if(strOutput == '')
            {
                for(List<BIIB_Work__c> lstWork :[SELECT Id FROM BIIB_Work__c WHERE RecordType.Name =:strWorkItemRecordType AND BIIB_Case__c =:strCaseId AND BIIB_Status__c = 'Closed-Failed'])
                {
                    // If the No Of attempts have  exceeded wrt the value in custom setting, then Navigate the user on the new Work Item creation page where RecordType is Escalate To Field
                    if(lstWork.size() == intNoOfAttempts)
                    {
                        strOutput = 'Navigate';
                        system.debug('--------strOutputNavigate------'+strOutput);
                        // Redirect Page to a new page
                    }
                    // The No Of attempts have not exceeded and the agent should create one more work item for this record type.
                    else
                    {
                        strOutput = 'CreateAdditionalWorkItem';
                        system.debug('--------strOutputCreateAdditionalItem------'+strOutput);
                    }
                }
            }// End if(strOutput == '')
       return strOutput;
    }
    
}