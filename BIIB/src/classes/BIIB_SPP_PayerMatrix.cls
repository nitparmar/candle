public class BIIB_SPP_PayerMatrix {

Public String strHealthCareAccount {get;set;}
Public Boolean isValidPatient {get;set;}
private BIIB_work__c Owork;
String patientId = '';
String strPatientPrdId = '';
List<BIIB_Affiliation__c> lstAffiliated = new List<BIIB_Affiliation__c>();
public case c = new case();
private List<BIIB_PreferredSPPPlan__c> lstPlan = new List<BIIB_PreferredSPPPlan__c>();

    public BIIB_SPP_PayerMatrix(ApexPages.StandardController controller) { 
        Owork =(BIIB_work__c)controller.getRecord();
        getDisplay();   
    }
    
    public void getDisplay()
    {
        BIIB_work__c w = [Select Id, Name,BIIB_Case__c from BIIB_work__c where Id= :Owork.Id];
        if (w!=null && w.BIIB_Case__c!= null ){
            c = [Select Id, AccountId,Account.RecordType.Name from Case where Id=:w.BIIB_Case__c];
        }
        patientId = c.AccountId;
        if(patientId != NULL && patientId !=''){
            Account a = [Select Id, Name,BIIB_Product__c from Account where Id = :patientId];
            strPatientPrdId = a.BIIB_Product__c;  
            if(c.Account.RecordType.Name == 'Patient')
            {
                isValidPatient = true;      
            }
        }
        
        List<BIIB_patient_plan__c> lstPatientPlan = new List<BIIB_patient_plan__c>();
        lstAffiliated = [SELECT Id, BIIB_End_Date__c,BIIB_To_Account__c FROM BIIB_Affiliation__c WHERE BIIB_From_Account__c =:patientId and BIIB_Active__c = true and BIIB_Affiliation_Type__c='Patient - SPP'];
        system.debug('**'+lstAffiliated);
        if(lstAffiliated.size() > 0)
            strHealthCareAccount = lstAffiliated[0].BIIB_To_Account__c;
        lstPatientPlan = [SELECT BIIB_Plan__c FROM BIIB_patient_plan__c WHERE BIIB_Patient_Lookup__c = :patientId];
        
        Set<ID> stPlanId = new Set<Id>();
        
        for(BIIB_patient_plan__c opp: lstPatientPlan)
        {
            stPlanId.add(opp.BIIB_plan__c);
        }
        system.debug('*'+stPlanId); 
        lstPlan = [SELECT Id, name, BIIB_Plan__c, BIIB_spp1__c,BIIB_spp1__r.Name,BIIB_spp2__c,BIIB_spp2__r.Name,BIIB_Product__c,BIIB_PBM__c, BIIB_PBM__r.Name FROM BIIB_PreferredSPPPlan__c WHERE BIIB_Plan__c IN : stPlanId and BIIB_Product__c = :strPatientPrdId order by BIIB_spp1__r.Name, BIIB_spp2__r.Name ];
        system.debug('*'+lstPlan);
        
    }
    public void Save()
    {
        if(strHealthCareAccount != Null)
        {
            if(lstAffiliated.size() > 0)
            {
                lstAffiliated[0].BIIB_End_Date__c = system.today();
                try{
                    update lstAffiliated[0];
                }catch(Exception e){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                    ApexPages.addMessage(myMsg);
                }
            }
       
            //create a new record with patient id and healthcare id and effective date as today.
       
            BIIB_Affiliation__c objAff=new BIIB_Affiliation__c(BIIB_From_Account__c =patientId,BIIB_Effective_Date__c=system.today(),BIIB_To_Account__c=strHealthCareAccount);
            try{
                insert objAff;
            }catch(Exception e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(myMsg);
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,' Record Saved Successfully ');
            ApexPages.addMessage(myMsg);
            getDisplay();
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Please Select Any One SPP ');
            ApexPages.addMessage(myMsg);
        }
    }
    public List<SelectOption> getItems() {
        Set<Id> SPPId = new Set<Id>();
        List<SelectOption> options = new List<SelectOption>(); 
        for(BIIB_PreferredSPPPlan__c objPlan : lstPlan){
            if(!SPPId.contains(objPlan.BIIB_PBM__c)){
               options.add(new SelectOption(String.valueOf(objPlan.BIIB_spp2__c),String.valueOf(objPlan.BIIB_PBM__r.Name) + ' (PBM)'));
               SPPId.add(objPlan.BIIB_PBM__c);
            }
        } 
        for(BIIB_PreferredSPPPlan__c objPlan : lstPlan){
            if(!SPPID.contains(objPlan.BIIB_spp1__c)){
                options.add(new SelectOption(String.valueOf(objPlan.BIIB_spp1__c),String.valueOf(objPlan.BIIB_spp1__r.Name) + ' (SSP1)'));
                SPPId.add(objPlan.BIIB_spp1__c);
            }
        } 
        for(BIIB_PreferredSPPPlan__c objPlan : lstPlan){
            if(!SPPId.contains(objPlan.BIIB_spp2__c)){
               options.add(new SelectOption(String.valueOf(objPlan.BIIB_spp2__c),String.valueOf(objPlan.BIIB_spp2__r.Name) + ' (SPP2)'));
               SPPId.add(objPlan.BIIB_spp2__c);
            }
        } 
        for (Account a : [Select Id, Name from Account where Recordtype.Name ='SPP' and Id NOT in :SPPId]){
            options.add(new SelectOption(a.Id, a.Name));
            SPPId.add(a.Id);
        }        
        system.debug('options -- ' + options);
        return options;
    }
     
}