/*****************************************************************************

Class Name  : BIIB_AccountTrigger_Handler
Created By  : Charles Mutsu
Description : Handle the Account Trigger Functions

*****************************************************************************/
public class BIIB_AccountTrigger_Handler {
    
    
    public static void OnAfterInsert(List<Account> newAccountList) {
        // Call the trigger handler to update Professional_vod Do Not Call field on Professional_vod's Patients
        BIIB_AccountTrigger_Handler.updateProfessionalDoNotCallOnPatient(newAccountList);
        BIIB_AccountTrigger_Handler.createLeadRecord(newAccountList);
    }
    
    public static void OnAfterUpdate(List<Account> newAccountList,Map<Id,Account> oldAccountMap) {
        // Call the trigger handler to update Professional_vod Do Not Call field on Professional_vod's Patients
        BIIB_AccountTrigger_Handler.updateProfessionalDoNotCallOnPatient(newAccountList);
        BIIB_AccountTrigger_Handler.createLeadRecordUpdate(newAccountList,oldAccountMap);
    }
    
    
    //Method Name   : updateProfessionalDoNotCallOnPatient
    //Created By    : Charles Mutsu
    //Parameters    : List of Account
    //Function      : Updates Patient Account's Professional_vod Do not call field as True when Professional_vod Account's Do not call field is True
    public static void updateProfessionalDoNotCallOnPatient(List<Account> lstAcc){
        
        Set<ID> setProfessional_vodAccIds = new Set<ID>();   //Set to collect Professional_vod account ids
        Set<ID> setPatientAccIds = new Set<ID>();   //Set to collect Patient account ids
        List<Account> lstPatientAcc = new List<Account>();  //Collect the List of Patient Account to update
        
        //Collect Professional_vod Account IDs from List of Account sent from Trigger
        for(Account acc : lstAcc){
            if(acc.BIIB_Account_Profile_Type__c == 'Professional_vod' && acc.PersonDoNotCall){
                setProfessional_vodAccIds.add(acc.Id);
                System.debug('Professional_vodAccIds   '+ setProfessional_vodAccIds);
            }
        }
        
        //Collect Patient Account IDs from List of Affiliations between Patient and Professional_vod
        if(setProfessional_vodAccIds.size()>0){
            for(BIIB_Affiliation__c aff:[Select BIIB_Affiliation_Type__c, BIIB_From_Account__c, BIIB_Primary__c, BIIB_To_Account__c from BIIB_Affiliation__c where BIIB_From_Account__c IN:setProfessional_vodAccIds AND BIIB_Affiliation_Type__c = 'Professional_vod - Patient' AND BIIB_Primary__c = True ]){
                if(aff.BIIB_To_Account__c != NULL){
                    setPatientAccIds.add(aff.BIIB_To_Account__c);
                }
            }
        }
        
        //Collect the List of Patient Accounts to be updated
        if(setPatientAccIds.size()>0){
            for(Account acc: [Select BIIB_Account_Profile_Type__c, BIIB_HCP_Do_Not_Call__c from Account where Id IN:setPatientAccIds AND BIIB_Account_Profile_Type__c = 'Patient']){
                acc.BIIB_HCP_Do_Not_Call__c = True;
                lstPatientAcc.add(acc); 
            }
        }
        
        //Update Patient Accounts with Professional_vod Do Not Call field
        if(lstPatientAcc.size()>0){
            update lstPatientAcc;
        }
    }
    
    
    //Method Name   : createLeadRecord
    //Created By    : Saurabh Shertukde
    //Parameters    : List of Account
    //Function      : Create a Lead record for the corresponding patient profile that initiates an interest in the TOUCH participation
    
    public static void createLeadRecord(List<Account> accList){
        
        
        List<Lead> leadLst=new List<Lead>();
        List<QueueSObject> temp=[Select queueId from QueueSObject where Queue.Name = 'IAM'];
         
        for(Account lstAcc: accList){
        
            if(lstAcc.BIIB_Interested_In_Touch__c && !lstAcc.BIIB_Touch_Participant__c){
            
            Lead tempLead = new Lead();
            tempLead.lastname=lstAcc.name;
            tempLead.City=lstAcc.PersonMailingCity;
            tempLead.Street=lstAcc.PersonMailingStreet;
            
            tempLead.ownerId=temp[0].queueId;
            leadLst.add(tempLead);
            
            }
            
        }
        insert leadLst;
    
    }
    
     //Method Name  : createLeadRecordUpdate
    //Created By    : Saurabh Shertukde
    //Parameters    : List of  new/updated Account records and Map of old Account records(before updation)
    //Function      : Create a Lead record for the corresponding patient profile that initiates an interest in the TOUCH participation
       
    
     public static void createLeadRecordUpdate(List<Account> accList,Map<Id,Account> oldaccMap){
        
        
        List<Lead> leadLst=new List<Lead>();       
        List<String> tempLeadName=new List<String>();
        for(Account tempAcc: accList){
        
        tempLeadName.add(tempAcc.name);
         
        }
        
        List<Lead> tempLst=[select Id,description,lastname,status from Lead where lastname IN:tempLeadName];
        List<QueueSObject> temp=[Select queueId from QueueSObject where Queue.Name = 'IAM'];
        List<Lead> updtLead=new List<Lead>();
        
       
         
        for(Account lstAcc: accList){
            //Check if Interested In TOUCH flag was initially true or false
            if(lstAcc.BIIB_Interested_In_Touch__c && !oldaccMap.get(lstAcc.Id).BIIB_Interested_In_Touch__c){
                //Get the Leads for the Corresdonping updated Account records to handle the Touch participation process
                for(Lead modLead:tempLst){
                    if(!lstAcc.BIIB_Touch_Participant__c && modLead.lastname == lstAcc.name && modLead.status!='Closed-Converted'){
                    
                        Lead tempLead = new Lead();
                        tempLead.lastname=lstAcc.name;
                        tempLead.City=lstAcc.PersonMailingCity;
                        tempLead.Street=lstAcc.PersonMailingStreet;
            
                        tempLead.ownerId=temp[0].queueId;
                        leadLst.add(tempLead);
                        }
                        else{
                        lstAcc.addError('The Infusion Site has already been registered as a touch participant!');                    
                        }
                }
            }
            if(!lstAcc.BIIB_Interested_In_Touch__c && oldaccMap.get(lstAcc.Id).BIIB_Interested_In_Touch__c){
                for(Lead modLead:tempLst){
                    if(modLead.lastname == lstAcc.name && modLead.status!='Closed-Not Converted' && modLead.status!='Closed-Converted'){
                
                    modLead.description='The Infusion Site has withdrawn its interest from participating in TOUCH';
                    modLead.status='Closed-Not Converted';
                    updtLead.add(modLead);
                    }
                    
                
                }
            try{
            update updtLead;
            }
            catch(Exception e){
            Lead leadError=new Lead();
            leadError.addError('The process encoutered the following error:'+e.getmessage());
            }
            
            }
            
        }
        try{
        insert leadLst;
        }
        catch(Exception e){
        Lead leadError=new Lead();
        leadError.addError('The process encoutered the following error:'+e.getmessage());
        }
        
        if(tempLst.isEmpty()){
            for(Account lstAcc: accList){
             Lead tempLead = new Lead();
             tempLead.lastname=lstAcc.name;
             tempLead.City=lstAcc.PersonMailingCity;
             tempLead.Street=lstAcc.PersonMailingStreet;
            
             tempLead.ownerId=temp[0].queueId;
             leadLst.add(tempLead);
             }
             try{
             insert leadLst;
             }
             catch(Exception e){
             Lead leadError=new Lead();
             leadError.addError('The process encoutered the following error:'+e.getmessage());
             }
        
        }
    
    }
    
    
    
    
}