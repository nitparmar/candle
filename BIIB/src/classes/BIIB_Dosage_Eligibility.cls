public class BIIB_Dosage_Eligibility {

    public String selectedProgram {get;set;}
    public Map<BIIB_Questionnaire__c, List<wQuestionnaire>> questionnaireMap {get;set;}
    public List<BIIB_Questionnaire__c> qList{get; set;}
    
    public BIIB_Dosage_Eligibility(ApexPages.StandardController controller){
        
        String[] programs = new String[]{'Quick Start','Sure Start'};
        this.programOptions = new SelectOption[]{};
        
        for (String c: programs ) {
            this.programOptions.add(new SelectOption(c,c));
        }
        
        String str = ApexPages.currentPage().getParameters().get('pgm');
        questionnaireMap = new map<BIIB_Questionnaire__c, List<wQuestionnaire>>();
        qlist = new list<BIIB_Questionnaire__c>();
        System.debug('hello'+str);
        if(str != NULL){
            for(BIIB_Questionnaire__c q:[Select ID, BIIB_Question__c, BIIB_Program__c,(Select ID, BIIB_Response__c, BIIB_Question__c from BIIB_Responses__r Order by ID asc) from BIIB_Questionnaire__c where BIIB_Program__c =:str Order By ID asc]){
            List<BIIB_Response__c> ansLst =  q.BIIB_Responses__r ;
            list<wQuestionnaire> qAnLst = new list<wQuestionnaire>();
              for (BIIB_Response__c an : ansLst) {
                 wQuestionnaire qAn = new wQuestionnaire();
                 qAn.listAns = an ;
                 qAn.selected = false ;
                 qAnLst.add(qAn);
             }
             questionnaireMap.put(q,qAnLst ); 
             qList.add(q); 
            }
            
        }
    }
    
    public SelectOption[] programOptions { //this is where we're going to pull the list
        public get;
        private set;
    }
    
    public PageReference next() {
    
      system.debug('Surely this one'+this.selectedProgram );
      PageReference pageRef = Page.BIIB_Questionnaire;      
      pageRef.getParameters().put('pgm',selectedProgram);
      return PageRef.setRedirect(True);  
   }
   public class wQuestionnaire {
        
        public BIIB_Response__c listAns {get; set;}
        public Boolean selected {get; set;}

        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public wQuestionnaire (){}
        public wQuestionnaire (BIIB_Response__c an) {
            
            listAns = an;
            selected = false;
        }
    }

}