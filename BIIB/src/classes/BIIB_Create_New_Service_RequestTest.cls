/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/21/2014
 * Author : Pruthvi Raj
 * Description : This Class is used for testing the functionality of  BIIB_Create_New_Service_Request .
 * 
 */
@isTest
public class BIIB_Create_New_Service_RequestTest {

    static testMethod void myUnitTest() {
    
    Account tempAcc =  BIIB_testClassHelper.createAccountPatient('accHandler','testpatient');
    insert tempAcc;
    
    Account tempAcc1 =  BIIB_testClassHelper.createAccountProfessionalVod('accHandler1','testpatient1');
    insert tempAcc1;
    
          
    BIIB_Affiliation__c testAffiliation = BIIB_testClassHelper.createAffiliation(tempAcc,tempAcc1);
    insert testAffiliation;
          
    Product_vod__c prod_vod = new Product_vod__c(Name = 'Tysabri');
    insert prod_vod;
              
    BIIB_Business_Rules_Configuration__c brc_rec = new BIIB_Business_Rules_Configuration__c(Name = 'Test BRC' , BIIB_Validation_Error__c = 'Test Error' , BIIB_Allow_more_than_one_active__c = True , BIIB_Account__c = tempAcc.id , BIIB_Product__c = 'Tysabri' , BIIB_Customer_Type__c = 'Patient');
    insert brc_rec;
    
   /* Therapy__c ther_rec_ret = new Therapy__c(BIIB_Product__c = prod_vod.id);
    insert ther_rec_ret;
    Therapy__c ther_rec = [Select Id,BIIB_Product__c From Therapy__c Where  Id=:ther_rec_ret.id];
    
    Patient_Therapy__c pt_rec= new Patient_Therapy__c( BIIB_Patient__c = tempAcc.id , BIIB_Therapy__c = ther_rec.id , BIIB_Primary__c = True);
    insert pt_rec;
    Patient_Therapy__c pt_rec_ret = [SELECT Id , Name , BIIB_Patient__c , BIIB_Therapy__c , BIIB_Primary__c FROM Patient_Therapy__c WHERE ID=:pt_rec.id];
    */
    BIIB_Work_Configuration__c work_config_rec = new BIIB_Work_Configuration__c(BIIB_Business_Rules_Configuration__c = brc_rec.id , BIIB_Priority__c = '80' , BIIB_Description__c = 'Test Desc' , BIIB_Required__c = True , /*BIIB_Route_Date__c = 'Test',*/ BIIB_Work_Group__c = 'Adherence' , BIIB_Work_Sub_Type__c = 'Sub' , BIIB_Work_Type__c = 'Type' , BIIB_Point_of_Contact__c = 'Patient' , BIIB_Related_To__c = tempAcc.id);
    insert work_config_rec;
    
    Pagereference mypgref = Page.BIIB_Create_New_Service_Request;
    Test.setCurrentPage(mypgref);
    Apexpages.currentPage().getParameters().put('Id', tempAcc.id);
    Apexpages.currentPage().getParameters().put('CustomerType','Patient');
    //ApexPages.currentPage().getParameters().put('ServiceRequestTemplate' , '');
    
    Test.startTest();
    BIIB_Create_New_Service_Request obj = new BIIB_Create_New_Service_Request();
    List<SelectOption> myOptions = new List<SelectOption>();
    //List<WorkItemConfiguration> myworkitemconfig = new List<WorkItemConfiguration>();
    //myOptions = obj.getServiceRequests();
    //myOptions = obj.getProductList();
    obj.getWorkItemConfiguration();
    obj.getAddRemoveConfiguration(); 
    PageReference pgref = obj.SelectProduct();
    ApexPages.currentPage().getParameters().put('ServiceRequestTemplate' , '');
    ApexPages.currentPage().getParameters().put('ServiceAction' , '');
    ApexPages.currentPage().getParameters().put('ServiceIndex' , '10');
    ApexPages.currentPage().getParameters().put('ProductName' , 'Tysabri');
    pgref = obj.SelectServiceRequest();
    
    PageReference pgref1 = obj.SaveCreateSR_WorkItems();
    
    pgref = obj.SelectAddRemoveConfiguration();
    //BIIB_Create_New_Service_Request.WorkItemConfiguration(10 , True , True , work_config_rec);
    Test.stopTest();
    }
}