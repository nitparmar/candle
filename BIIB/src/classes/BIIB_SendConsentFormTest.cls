/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/24/2014
 * Author : Pruthvi Raj
 * Description : This Class is used for testing the functionality of  BIIB_SendConsentFormTest .
 * 
 */
 
@isTest
public class BIIB_SendConsentFormTest {

    public static testMethod void myUnitTest() {
    	
    	RecordType acc_rectype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND DeveloperName  = 'BIIB_HCP'];
        Account acc_hcp_rec = new Account(FirstName = 'Test' , LastName = 'Name' , RecordTypeId = acc_rectype.id , PersonDoNotCall = true);
    	insert acc_hcp_rec ;
		BIIB_Send_Consent__c consent_rec = new BIIB_Send_Consent__c(Consent_value__c = '--None--' , Name= 'Test');
		insert 	consent_rec;
		
		Test.startTest();
    	ApexPages.currentPage().getParameters().put('Record',acc_hcp_rec.id);
    	BIIB_SendConsentForm obj = new BIIB_SendConsentForm(new ApexPages.StandardController(acc_hcp_rec));      
    	List<SelectOption> myOptions = new List<SelectOption>();
    	myOptions = obj.getConsentName();
    	Test.stopTest();         
    }
}