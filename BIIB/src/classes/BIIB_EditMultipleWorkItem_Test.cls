/**
*@author Aditya Ghosh
*@date 22/04/2014
@test class for BIIB_EditMultipleWorkItem
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Aditya       22/04/2014  OriginalVersion
*/

@isTest
public class BIIB_EditMultipleWorkItem_Test{
    
    public List<Case> lstCase;
    public static List<RecordType> case_rectype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Case' AND Name  in ('12 Week Questionnaire' , 'Adverse Event' , 'Benefit Investigation' , 'Financial Assistance' , 'Medicare Outreach' , 'Patient Check-In' , 'Support')  ORDER BY Name];
    public static User tempUser = [Select id from User limit 1];
    public static testmethod void testFunction(){
    test.startTest();
        Case tempCase = BIIB_testClassHelper.createCase();
        insert tempCase;
        
        BIIB_Work__c tempWork = BIIB_testClassHelper.createWork(tempUser , tempCase);
        insert tempWork;
        
        ApexPages.StandardController sc = new ApexPages.standardController(tempCase);
        BIIB_EditMultipleWorkItem testInstance = new BIIB_EditMultipleWorkItem(sc);
        testInstance.save();
        //Cover Exceptions
        BIIB_Work__c tempWork_exception = [Select id from BIIB_Work__c where BIIB_Case__c=:tempCase.Id];
        Delete tempWork_exception ;
        testInstance.save();
    test.stopTest();
    }
    
}