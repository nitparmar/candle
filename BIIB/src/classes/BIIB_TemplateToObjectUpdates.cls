/*  @Name: BIIB_TemplateToObjectUpdates
    @Author: Divya Galani 
    @Created Date:16-Apr-14
    @Description:PRE Functionality: US-0072
       1. It has 2 methods: BIIB_UpdateWorkItems_FromWITemplate and BIIB_UpdateCases_FromSRTemplate
       2. Whenever a Work Item Template is updated, all its corresponsing Work Items should get updated.This is done in method: BIIB_UpdateWorkItems_FromWITemplate
       3. Whenever a SR Template is updated, all its corresponsing Cases should get updated.This is done in method: BIIB_UpdateCases_FromSRTemplate
       
       Modification Log
       -----------------------------------------------------------------------------------------------
       Developer                Date                Description
       Divya Galani             16-Apr-14           Original Version
       Divya Galani             20-Apr-14           Added Method BIIB_UpdateWorkItems_FromWITemplate
       -----------------------------------------------------------------------------------------------
       
*/

public with sharing class BIIB_TemplateToObjectUpdates
{
    /*
    Map<Id,Map<String,String>> mapUpdatedSRTemplates = new Map<Id,Map<String,String>>();
    Map<Id,Map<String,String>> mapUpdatedWITemplates = new Map<Id,Map<String,String>>();
    Map<Id,Integer> mapTemplateDependency = new Map<Id,Integer>();
    Set<Id> setAccountId = new Set<Id>();
    Set<Double> setSequence = new Set<Double>();
    Map<String,Map<Double,Id>> mapParentCases = new Map<String,Map<Double,Id>>();
    
    
    public void BIIB_UpdateWorkItems_FromWITemplate(List<BIIB_Work_Item_Template__c> lstnewRecords,Map<Id, BIIB_Work_Item_Template__c> mapoldRecords)
    {
        // Get All Field Data From WorkItem_To_Work Custom Setting
        List<BIIB_PRE_Work_Mapping__c> lstWorkItemTemplateToWork = [SELECT BIIB_Work_Field__c,BIIB_Work_Template_Field__c FROM BIIB_PRE_Work_Mapping__c];
        
        for(BIIB_Work_Item_Template__c objWorkTemplateNew : lstnewRecords) 
        {
            SObject objWorkItemTemplateNew = objWorkTemplateNew;
            Schema.sObjectType objectDefWorkNew = Schema.getGlobalDescribe().get('Work__c').getDescribe().getSObjectType();
            SObject objWorkTemplateNewData = objectDefWorkNew.newSObject();
            
            // Check in the map of oldrecord if it contains the RowId of the Work Item Templates which has been updated.
            if(mapoldRecords.containsKey(objWorkTemplateNew.Id))
            {
                for(BIIB_PRE_Work_Mapping__c objCustomSetting :lstWorkItemTemplateToWork)
                {
                    BIIB_Work_Item_Template__c objOldWorkItemTemplateData = mapoldRecords.get(objWorkTemplateNew.Id);
                    SObject objOldWorkItemTemplate = objOldWorkItemTemplateData;
                    Schema.sObjectType objectDefWIOld = Schema.getGlobalDescribe().get('Work__c').getDescribe().getSObjectType();
                    SObject objWITemplateOldData = objectDefWIOld.newSObject();
                    
                    // This Loop is to check the Other set of Work Item Template Fields. If the Field Value is modified, then Create a Map<WITemplateId, <Map<UpdatedField,UpdatedFieldValue>>
                    if(String.valueOf(objWorkItemTemplateNew.get(objCustomSetting.BIIB_Work_Template_Field__c)) !=  String.valueOf(objOldWorkItemTemplate.get(objCustomSetting.BIIB_Work_Template_Field__c))) 
                    {
                        if(!mapUpdatedWITemplates.containsKey(objWorkTemplateNew.Id))
                        {
                            Map<String,String> mapUpdatedFields = new Map<String,String>();
                            mapUpdatedFields.put(objCustomSetting.BIIB_Work_Field__c,String.valueOf(objWorkItemTemplateNew.get(objCustomSetting.BIIB_Work_Template_Field__c)));
                            mapUpdatedWITemplates.put(objWorkTemplateNew.Id,mapUpdatedFields);
                            
                        }
                        else
                        {
                            mapUpdatedWITemplates.get(objWorkTemplateNew.Id).put(objCustomSetting.BIIB_Work_Field__c,String.valueOf(objWorkItemTemplateNew.get(objCustomSetting.BIIB_Work_Template_Field__c)));
                        }
                    }
                }
            }
        }

        
        // Update Fields On Work
        // Query All Work Item Records where WITemplateId exists in the mapUpdatedWITemplates.i.e Query all Work Items for the WI Templates which have been updated and loop through it
        for(String Key :mapUpdatedWITemplates.keySet())
        {

            List<Work__c> lstWorkItems = new List<Work__c>([SELECT Id from Work__c where BIIB_Work_Item_Template__c =: Key]);
            String strObjectName = 'Work__c';
            
            Schema.SObjectType Res = Schema.getGlobalDescribe().get(strObjectName);
            Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
            
            for(Work__c objWork :lstWorkItems)
            {
                //Inside For Loop
                SObject objWorkGenericObject = objWork;
                Map<String,String> mapFieldNameValue= new Map<String,String>();
                mapFieldNameValue = mapUpdatedWITemplates.get(Key);

                // Create Work Object and Update field values for whatever corresponding fields that have been modified on the WI Template.
                //Here, check for all the Data Types is done as type casting will be needed based on field data type
                for(String KeyName :mapFieldNameValue.keySet())
                {
                    
                    Schema.DisplayType strFieldDataType=fieldMap.get(KeyName).getDescribe().getType();
                    String strFieldDataTypeConverted =  String.valueOf(fieldMap.get(KeyName).getDescribe().getType());
                    
                    if(strFieldDataTypeConverted == 'BOOLEAN')
                    {
                        if(mapFieldNameValue.get(KeyName) != NULL)
                        {
                            objWorkGenericObject.put(KeyName,Boolean.valueOf(mapFieldNameValue.get(KeyName)));
                        }
                        else
                        {
                            objWorkGenericObject.put(KeyName,NULL);
                        }
                    }
                    
                    else if(strFieldDataTypeConverted == 'STRING')
                    {
                        if(mapFieldNameValue.get(KeyName) != NULL)
                        {
                            objWorkGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                        }
                        else
                        {
                            objWorkGenericObject.put(KeyName,NULL);
                        }
                    }
                    
                    else if(strFieldDataTypeConverted == 'INTEGER')
                    {
                        if(mapFieldNameValue.get(KeyName) != NULL)
                        {
                            objWorkGenericObject.put(KeyName,Integer.valueOf(mapFieldNameValue.get(KeyName)));
                        }
                        else
                        {
                            objWorkGenericObject.put(KeyName,NULL);
                        }
                    }
                    else if(strFieldDataTypeConverted == 'DOUBLE')
                    {
                        if(mapFieldNameValue.get(KeyName) != NULL)
                        {
                            objWorkGenericObject.put(KeyName,Double.valueOf(mapFieldNameValue.get(KeyName)));
                        }
                        else
                        {
                            objWorkGenericObject.put(KeyName,NULL);
                        }
                    }
                    else
                    {
                        objWorkGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                    }
                }
                
                Work__c objFinalWork = (Work__c)objWorkGenericObject;
                system.debug('-----------objFinalWork-------------'+objFinalWork);
                update objFinalWork;
            }
        }       
    }
    /**********************************************BIIB_UpdateCases_FromSRTemplate***********************************************************
    public void BIIB_UpdateCases_FromSRTemplate(List<BIIB_SR_Template__c> lstnewRecords,Map<Id, BIIB_SR_Template__c> mapoldRecords)
    {

        List<BIIB_SRTemplate_To_SR_Mapping__c> lstSRTemplateToSR = [SELECT BIIB_SR_Field__c,BIIB_SR_Template_Field__c FROM BIIB_SRTemplate_To_SR_Mapping__c];
        for(BIIB_SR_Template__c objSRTemplateNew : lstnewRecords) 
        {
            SObject objSRTemplateNewObject = objSRTemplateNew;
            Schema.sObjectType objectDefSRTNew = Schema.getGlobalDescribe().get('BIIB_SR_Template__c').getDescribe().getSObjectType();
            SObject objSRTemplateNewData = objectDefSRTNew.newSObject();
            
            // Check in the map of oldrecord if it contains the RowId of the SRtemplates which has been updated.
            if(mapoldRecords.containsKey(objSRTemplateNew.Id))
            {
                for(BIIB_SRTemplate_To_SR_Mapping__c objCustomSetting :lstSRTemplateToSR)
                {
                    BIIB_SR_Template__c objOldSRTemplateData = mapoldRecords.get(objSRTemplateNew.Id);
                    SObject objOldSRT = objOldSRTemplateData;
                    Schema.sObjectType objectDefSRTOld = Schema.getGlobalDescribe().get('BIIB_SR_Template__c').getDescribe().getSObjectType();
                    SObject objSRTemplateOldData = objectDefSRTOld.newSObject();

                    // If the Dependency Field has been updated, then Create a Map of the SRTemplateId and the updated Dependency Value
                    if(objCustomSetting.BIIB_SR_Template_Field__c == 'BIIB_Dependency__c')
                    {
                        mapTemplateDependency.put(objSRTemplateNew.Id,Integer.valueOf(objSRTemplateNew.BIIB_Dependency__c));
                        
                        // Create a Set and Put the updated Dependency Values on each of the SR Templates
                        setSequence.add(Integer.valueOf(objSRTemplateNew.BIIB_Dependency__c));

                    }
                    
                    // This Loop is to check the Other set of SR Template Fields. If the Field Value is modified, then Create a Map<SRTemplateId, <Map<UpdatedField,UpdatedFieldValue>>
                    if(String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)) !=  String.valueOf(objOldSRT.get(objCustomSetting.BIIB_SR_Template_Field__c))) 
                    {
                        if(!mapUpdatedSRTemplates.containsKey(objSRTemplateNew.Id))
                        {
                            Map<String,String> mapUpdatedFields = new Map<String,String>();
                            mapUpdatedFields.put(objCustomSetting.BIIB_SR_Field__c,String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)));
                            mapUpdatedSRTemplates.put(objSRTemplateNew.Id,mapUpdatedFields);
                            
                        }
                        else
                        {
                            mapUpdatedSRTemplates.get(objSRTemplateNew.Id).put(objCustomSetting.BIIB_SR_Field__c,String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)));
                        }
                    }
                }
            }
        }

        
        // Update Fields On Case
        // Query All Cases where SRTemplateId exists in the mapUpdatedSRTemplates.i.e Query all cases for the SR Templates which have been updated and loop through it
        for(String Key :mapUpdatedSRTemplates.keySet())
        {

            List<Case> lstCases = new List<Case>([SELECT Id from Case where BIIB_SR_Template__c =: Key]);
            String strObjectName = 'Case';
            
            Schema.SObjectType Res = Schema.getGlobalDescribe().get(strObjectName);
            Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
            
            for(Case objCase :lstCases)
            {
                //Inside For Loop
                SObject objCaseGenericObject = objCase;
                Map<String,String> mapFieldNameValue= new Map<String,String>();
                mapFieldNameValue = mapUpdatedSRTemplates.get(Key);

                // Create Case Object and Update field values for whatever corresponding fields that have been modified on the SR Template.
                //Here, check for all the Data Types is done as type casting will be needed based on field data type
                for(String KeyName :mapFieldNameValue.keySet())
                {
                    
                    Schema.DisplayType strFieldDataType=fieldMap.get(KeyName).getDescribe().getType();
                    String strFieldDataTypeConverted =  String.valueOf(fieldMap.get(KeyName).getDescribe().getType());
                    
                    if(strFieldDataTypeConverted == 'BOOLEAN')
                    {
                        objCaseGenericObject.put(KeyName,Boolean.valueOf(mapFieldNameValue.get(KeyName)));
                    }
                    
                    else if(strFieldDataTypeConverted == 'STRING')
                    {
                        objCaseGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                    }
                    
                    else if(strFieldDataTypeConverted == 'INTEGER')
                    {
                        objCaseGenericObject.put(KeyName,Integer.valueOf(mapFieldNameValue.get(KeyName)));
                    }
                    else if(strFieldDataTypeConverted == 'DOUBLE')
                    {
                        objCaseGenericObject.put(KeyName,Double.valueOf(mapFieldNameValue.get(KeyName)));
                    }
                    else
                    {
                        objCaseGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                    }
                }
                Case objFinalCase = (Case)objCaseGenericObject;
                update objFinalCase;
            }
        }
            
        // Update Parent Case Information Based on SR Template Updates
        
            // This is the List Of Cases for which the "Dependency" field has been updated on its related SR Template
            List<Case> lstCasesDependency = new List<Case>([SELECT Id,AccountId from Case where BIIB_SR_Template__c IN :mapTemplateDependency.keySet()]);
            
            
            //Based on the above list, we need to filter out the unique set of AccountId's for which their related SR's need to be updated for dependency functionality
            for(Case objCaseDetails : lstCasesDependency)
            {
                setAccountId.add(objCaseDetails.AccountId);
            }

            //  Query SR's where Seqeuence is in setSequence and AccountId in setAccountId. This will basically create a Map<AccountId, Map<ParentCaseSequenceNumber,ParentCaseId>>
            for (Case objParentCases: [SELECT Id,BIIB_Sequence_Number__c,AccountId,BIIB_Dependency__c FROM Case WHERE BIIB_Sequence_Number__c IN :setSequence AND AccountId IN :setAccountId AND Id NOT IN :lstCasesDependency])
            {
                //Map of <AccountId, <Map<SequenceNum,CaseId>>
                Map<Double,Id> mapSequenceCaseId = new Map<Double,Id>();
                mapSequenceCaseId.put(Double.valueOf(objParentCases.BIIB_Sequence_Number__c),objParentCases.Id);
                
                if(!mapParentCases.containsKey(objParentCases.AccountId))
                {
                    mapParentCases.put(objParentCases.AccountId,mapSequenceCaseId);
                }
                
                else
                {
                    mapParentCases.get(objParentCases.AccountId).put(objParentCases.BIIB_Sequence_Number__c,objParentCases.Id);
                }
            }

            // Query Cases where Account is in the Set
            for(List<Case> lstAccountCases: [SELECT Id,BIIB_Dependency__c,BIIB_Sequence_Number__c,AccountId FROM Case where AccountId IN :setAccountId])
            {
                for(Case objCaseUpdates :lstAccountCases)
                {
                    // Fetch the Value Of Dependency On the Case
                    Double intDependency = objCaseUpdates.BIIB_Dependency__c;
                    if(mapParentCases.containsKey(objCaseUpdates.AccountId))
                    {
                        //Fetch Parent CaseId and update on Child Case
                        Map<Double,Id> mapParentSequenceCaseId = new Map<Double,Id>();
                        mapParentSequenceCaseId = mapParentCases.get(objCaseUpdates.AccountId);
                        if(mapParentSequenceCaseId.containsKey(intDependency))
                        {
                            String strParentCaseId = mapParentSequenceCaseId.get(intDependency);
                            objCaseUpdates.ParentId = strParentCaseId;
                            update objCaseUpdates;
                            
                        }
                    }
                }
            }
    }
    */
}