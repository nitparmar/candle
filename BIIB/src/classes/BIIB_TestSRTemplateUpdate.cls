public with sharing class BIIB_TestSRTemplateUpdate 
{
	
    Map<Id,Map<String,String>> mapUpdatedSRTemplates = new Map<Id,Map<String,String>>();
    Map<Id,Integer> mapTemplateDependency = new Map<Id,Integer>();
    Set<Id> setAccountId = new Set<Id>();
    Set<Double> setSequence = new Set<Double>();
    Map<String,Map<Double,Id>> mapParentCases = new Map<String,Map<Double,Id>>();
    
    public void BIIB_UpdateCases_FromSRTemplate(List<BIIB_SR_Template__c> lstnewRecords,Map<Id, BIIB_SR_Template__c> mapoldRecords)
    {
		system.debug('------lstnewRecords----------'+lstnewRecords);
		system.debug('------mapoldRecords----------'+mapoldRecords);
    	List<BIIB_SRTemplate_To_SR_Mapping__c> lstSRTemplateToSR = [SELECT BIIB_SR_Field__c,BIIB_SR_Template_Field__c FROM BIIB_SRTemplate_To_SR_Mapping__c];
    	system.debug('------lstSRTemplateToSR----------'+lstSRTemplateToSR);
        for(BIIB_SR_Template__c objSRTemplateNew : lstnewRecords) 
        {
            SObject objSRTemplateNewObject = objSRTemplateNew;
            Schema.sObjectType objectDefSRTNew = Schema.getGlobalDescribe().get('BIIB_SR_Template__c').getDescribe().getSObjectType();
			SObject objSRTemplateNewData = objectDefSRTNew.newSObject();
			system.debug('--------objSRTemplateNewObject-------------'+objSRTemplateNewObject);
			system.debug('--------objSRTemplateNewData-------------'+objSRTemplateNewData);
			
            system.debug('------objSRTemplateNew.Id----------'+objSRTemplateNew.Id);
			if(mapoldRecords.containsKey(objSRTemplateNew.Id))
			{
                for(BIIB_SRTemplate_To_SR_Mapping__c objCustomSetting :lstSRTemplateToSR)
                {
                    BIIB_SR_Template__c objOldSRTemplateData = mapoldRecords.get(objSRTemplateNew.Id);
                    system.debug('------objOldSRTemplateData----------'+objOldSRTemplateData);
                    
					SObject objOldSRT = objOldSRTemplateData;
           			Schema.sObjectType objectDefSRTOld = Schema.getGlobalDescribe().get('BIIB_SR_Template__c').getDescribe().getSObjectType();
					SObject objSRTemplateOldData = objectDefSRTOld.newSObject();
					
					system.debug('------objCustomSettingSRTemplateFieldName----------'+objCustomSetting.BIIB_SR_Template_Field__c);
					system.debug('-------1stPartOfIfCondition-----'+objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c));
					system.debug('-------2ndPartOfIfCondition-----'+objOldSRT.get(objCustomSetting.BIIB_SR_Template_Field__c));
					system.debug('-------1stCondStringValueOf-----'+String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)));
					system.debug('-------2ndCondStringValueOf-----'+String.valueOf(objOldSRT.get(objCustomSetting.BIIB_SR_Template_Field__c)));
					
					if(objCustomSetting.BIIB_SR_Template_Field__c == 'BIIB_Dependency__c')
					{
						
						mapTemplateDependency.put(objSRTemplateNew.Id,Integer.valueOf(objSRTemplateNew.BIIB_Dependency__c));
						system.debug('------mapTemplateDependencyInsideLoop----------'+mapTemplateDependency);
						// Create a Set and Put the updated Dependency Values on each of the SR Templates
						setSequence.add(Integer.valueOf(objSRTemplateNew.BIIB_Dependency__c));
						system.debug('------setSequenceInsideLoop----------'+setSequence);
					}
					
                    if(String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)) !=  String.valueOf(objOldSRT.get(objCustomSetting.BIIB_SR_Template_Field__c))) 
                  	{
                        system.debug('------InsideIFCondition----------');
	                    
                        
                        if(!mapUpdatedSRTemplates.containsKey(objSRTemplateNew.Id))
                        {
                        	Map<String,String> mapUpdatedFields = new Map<String,String>();
                        	mapUpdatedFields.put(objCustomSetting.BIIB_SR_Field__c,String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)));
                        	system.debug('------mapUpdatedFields----------'+mapUpdatedFields);
	                       	mapUpdatedSRTemplates.put(objSRTemplateNew.Id,mapUpdatedFields);
	                      	system.debug('-----------IFmapUpdatedSRTemplates-------------------'+mapUpdatedSRTemplates);
                        }
                        else
                        {
                        	mapUpdatedSRTemplates.get(objSRTemplateNew.Id).put(objCustomSetting.BIIB_SR_Field__c,String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)));
                        	system.debug('-----------ELSEmapUpdatedSRTemplates-------------------'+mapUpdatedSRTemplates);
                        }
                    }
            	}
	     	}
        }

        
        // Update Fields On Case
        for(String Key :mapUpdatedSRTemplates.keySet())
        {
        	system.debug('---------Key---------'+Key);
        	List<Case> lstCases = new List<Case>([SELECT Id from Case where BIIB_SR_Template__c =: Key]);
        	system.debug('-----------lstCases-------------------'+lstCases);
        	String strObjectName = 'Case';
        	
			Schema.SObjectType Res = Schema.getGlobalDescribe().get(strObjectName);
			Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
        	
        	for(Case objCase :lstCases)
        	{
        		//Inside For Loop
        		SObject objCaseGenericObject = objCase;
        		Map<String,String> mapFieldNameValue= new Map<String,String>();
        		mapFieldNameValue = mapUpdatedSRTemplates.get(Key);
        		system.debug('---------mapFieldNameValue---------'+mapFieldNameValue);
        		for(String KeyName :mapFieldNameValue.keySet())
        		{
        			system.debug('---------KeyName---------'+KeyName);
        			system.debug('---------mapFieldNameValue.get(KeyName)---------'+mapFieldNameValue.get(KeyName));
        			
					Schema.DisplayType strFieldDataType=fieldMap.get(KeyName).getDescribe().getType();
                  	String strFieldDataTypeConverted =  String.valueOf(fieldMap.get(KeyName).getDescribe().getType());
                  	
                  	if(strFieldDataTypeConverted == 'BOOLEAN')
                  	{
	        			objCaseGenericObject.put(KeyName,Boolean.valueOf(mapFieldNameValue.get(KeyName)));
	        			system.debug('---------objCaseGenericObject---------'+objCaseGenericObject);
                  	}
                  	
                  	else if(strFieldDataTypeConverted == 'STRING')
                  	{
                  		objCaseGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                  	}
                  	
					else if(strFieldDataTypeConverted == 'INTEGER')
                  	{
                  		objCaseGenericObject.put(KeyName,Integer.valueOf(mapFieldNameValue.get(KeyName)));
                  	}
					else if(strFieldDataTypeConverted == 'DOUBLE')
                  	{
                  		objCaseGenericObject.put(KeyName,Double.valueOf(mapFieldNameValue.get(KeyName)));
                  	}
                  	else
                  	{
                  		objCaseGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                  	}
        		}
        		Case objFinalCase = (Case)objCaseGenericObject;
        		system.debug('---------objFinalCase---------'+objFinalCase);
        		update objFinalCase;
        	}
        }
        
                
	        system.debug('------mapTemplateDependencyOutsideLoop----------'+mapTemplateDependency);
	        system.debug('------setSequenceOutsideLoop----------'+setSequence);
	        
        // Update Parent Case Information Based on SR Template Updates
        
        	// This is the List Of Cases for which the "Dependency" field has been updated on its related SR Template
        	List<Case> lstCasesDependency = new List<Case>([SELECT Id,AccountId from Case where BIIB_SR_Template__c IN :mapTemplateDependency.keySet()]);
        	system.debug('---------lstCasesDependency---------'+lstCasesDependency);
        	
        	//Based on the above list, we need to filter out the unique set of AccountId's for which their related SR's need to be updated for dependency functionality
        	for(Case objCaseDetails : lstCasesDependency)
        	{
        		setAccountId.add(objCaseDetails.AccountId);
        	}
        	system.debug('---------setAccountId---------'+setAccountId);
        	
        	//  Query SR's where Seqeuence is in setSequence and AccountId in setAccountId
        	for (Case objParentCases: [SELECT Id,BIIB_Sequence_Number__c,AccountId,BIIB_Dependency__c FROM Case WHERE BIIB_Sequence_Number__c IN :setSequence AND AccountId IN :setAccountId AND Id NOT IN :lstCasesDependency])
        	{
	        	//Map of <AccountId, <Map<SequenceNum,CaseId>>
	        	Map<Double,Id> mapSequenceCaseId = new Map<Double,Id>();
				mapSequenceCaseId.put(Double.valueOf(objParentCases.BIIB_Sequence_Number__c),objParentCases.Id);
				
				if(!mapParentCases.containsKey(objParentCases.AccountId))
				{
	        		mapParentCases.put(objParentCases.AccountId,mapSequenceCaseId);
	        		system.debug('---------ParentCasesIFInsideLoop---------'+mapParentCases);
				}
				
				else
				{
					mapParentCases.get(objParentCases.AccountId).put(objParentCases.BIIB_Sequence_Number__c,objParentCases.Id);
					system.debug('---------ParentCasesElseInsideLoop---------'+mapParentCases);
				}
        	}
        	system.debug('---------ParentCasesIFOutsideLoop---------'+mapParentCases);
        	system.debug('---------ParentCasesElseOutsideLoop---------'+mapParentCases);
        	
        	for(List<Case> lstAccountCases: [SELECT Id,BIIB_Dependency__c,BIIB_Sequence_Number__c,AccountId FROM Case where AccountId IN :setAccountId])
        	{
	        	system.debug('---------lstAccountCases---------'+lstAccountCases);
	        	for(Case objCaseUpdates :lstAccountCases)
	        	{
	        		
	        		system.debug('---------objCaseUpdates---------'+objCaseUpdates);
	        		
	        		// Fetch the Value Of Dependency On the Case
	        		Double intDependency = objCaseUpdates.BIIB_Dependency__c;
	        		system.debug('---------intDependency---------'+intDependency);
	        		
					if(mapParentCases.containsKey(objCaseUpdates.AccountId))
					{
						system.debug('---------InsidemapParentCases.containsKey---------');
						Map<Double,Id> mapParentSequenceCaseId = new Map<Double,Id>();
						mapParentSequenceCaseId = mapParentCases.get(objCaseUpdates.AccountId);
						
						system.debug('---------mapParentSequenceCaseId---------'+mapParentSequenceCaseId);
						
						if(mapParentSequenceCaseId.containsKey(intDependency))
						{
							system.debug('---------mapParen////tSequenceCaseId.containsKey---------');
							String strParentCaseId = mapParentSequenceCaseId.get(intDependency);
							system.debug('---------strParentCaseId---------'+strParentCaseId);
							
							objCaseUpdates.ParentId = strParentCaseId;
							system.debug('---------BEFOREobjCaseUpdates---------'+objCaseUpdates);
							update objCaseUpdates;
							system.debug('---------AFTERobjCaseUpdates---------'+objCaseUpdates);
						}
					}
	        	}
        	}
    }
}