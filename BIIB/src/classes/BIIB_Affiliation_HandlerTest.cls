/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/22/2014
 * Author : Pruthvi Raj
 * Description : This Class is used for testing the functionality of  BIIB_Affiliation_Handler .
 * 
 */
 
@isTest
public class BIIB_Affiliation_HandlerTest{
 /*
     * 
     * Description : This method used to test the functionality of BIIB_Affiliation_Handler class 
    */

        public static testMethod void positive_test_case(){
        
          List<Account> lst_acc = new List<Account>();
          
          // Creating Test data
          Account tempAcc =  BIIB_testClassHelper.createAccountPatient('accHandler','testpatient');
          lst_acc.add(tempAcc);
          tempAcc = BIIB_testClassHelper.createAccountProfessionalVod('accHandler','testpro');
          lst_acc.add(tempacc);
          tempAcc = BIIB_testClassHelper.createAccountSPP('accHandler');
          lst_acc.add(tempacc);
          tempAcc = BIIB_testClassHelper.createAccountInfusionSites('accHandler');
          lst_acc.add(tempacc);
          tempAcc = BIIB_testClassHelper.createAccountPayer('accHandler');
          lst_acc.add(tempacc);
          tempAcc = BIIB_testClassHelper.createAccountHCO('accHandler');
          lst_acc.add(tempacc);
          tempAcc =  BIIB_testClassHelper.createAccountPatient('accHandler','testpatient1');
          lst_acc.add(tempAcc);
          tempAcc =  BIIB_testClassHelper.createAccountPatient('accHandler','testpatient2');
          lst_acc.add(tempAcc);
          insert lst_acc;
          
          //Added so that Patient_HCP_Primary_Validation does not fire on Association object
          lst_acc[1].PersonDoNotCall = False;
          update lst_acc;
          
          User tempUser = [Select id from User limit 1];
         
          Case tempCase_or = BIIB_testClassHelper.createCase();
          insert tempCase_or;
          Case tempCase = [SELECT Id,Status,BIIB_Is_Patient_DND__c, AccountId FROM Case where Id=:tempCase_or.id];
          
          BIIB_Work__c tempWork = BIIB_testClassHelper.createWork(tempUser , tempCase );
          insert tempWork;
          tempwork.BIIB_Status__c = 'Completed';
          update tempWork;
          BIIB_Work__c tempWork_ret = [SELECT Id,Name , BIIB_AccountRecordType__c , BIIB_Account__c ,  BIIB_Assigned_to__c , BIIB_Status__c , BIIB_Preferred_Language__c , BIIB_Case__c FROM BIIB_Work__c where Id=:tempWork.id];
        
          BIIB_Affiliation__c testAffiliation = BIIB_testClassHelper.createAffiliation(lst_acc[0],lst_acc[1]);
          insert testAffiliation;
          testAffiliation.BIIB_Preferred__c = false;
          testAffiliation.BIIB_End_Date__c = system.today()+1;
          testAffiliation.BIIB_Primary__c = false;
          testAffiliation.BIIB_Effective_Date__c = date.newInstance(2014, 4, 19);
          update testAffiliation;          
          testAffiliation = BIIB_testClassHelper.createAffiliation(lst_acc[0],lst_acc[3]);
          insert testAffiliation;
          testAffiliation = BIIB_testClassHelper.createAffiliation(lst_acc[0],lst_acc[2]);
          insert testAffiliation;
          testAffiliation = BIIB_testClassHelper.createAffiliation(lst_acc[4],lst_acc[3]);
          insert testAffiliation;
          testAffiliation = BIIB_testClassHelper.createAffiliation(lst_acc[1],lst_acc[3]);
          insert testAffiliation;
          testAffiliation = BIIB_testClassHelper.createAffiliation(lst_acc[1],lst_acc[0]);
          insert testAffiliation;
          testAffiliation = new BIIB_Affiliation__c(BIIB_Effective_Date__c = system.today()-1 , BIIB_End_Date__c = system.today()+300 , BIIB_To_Account__c = lst_acc[1].id , BIIB_From_Account__c = lst_acc[6].id, BIIB_Preferred__c = True  , BIIB_Primary__c = True, RecordTypeId=BIIB_Utility_Class.getRecordTypeId('BIIB_Affiliation__c', 'Patient - Professional'));
          insert testAffiliation;
          update testAffiliation;
          testAffiliation = new BIIB_Affiliation__c(BIIB_Effective_Date__c = system.today()-1 , BIIB_End_Date__c = system.today()+300 , BIIB_To_Account__c = lst_acc[2].id , BIIB_From_Account__c = lst_acc[7].id, BIIB_Preferred__c = True  , BIIB_Primary__c = True, RecordTypeId=BIIB_Utility_Class.getRecordTypeId('BIIB_Affiliation__c', 'Patient - Professional'));
          insert testAffiliation;
          update testAffiliation;
            
        }
        
}