/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/21/2014
 * Author : Pruthvi Raj
 * Description : This Class is used for testing the functionality of  BIIB_AccountTrigger_Handler .
 * 
 */
 
@isTest(seeAlldata = True)
public class BIIB_AccountTrigger_HandlerTest{
        /*
         * 
         * Description : This method tests the desired functionality of BIIB_AccountTrigger_Handler Class and BIIB_AccountTrigger Trigger
         * 
         */

        public static testMethod void positive_Test(){
            test.startTest();
                
                List<Account> lst_acc = new List<Account>();
                Account tempAcc =  BIIB_testClassHelper.createAccountPatient('accHandler','testpatient');
                lst_acc.add(tempAcc );
                tempAcc = BIIB_testClassHelper.createAccountProfessionalVod('accHandler','testpro');
                lst_acc.add(tempacc );
                
                insert lst_acc;
                //Added so that Patient_HCP_Primary_Validation does not fire on Association object
                lst_acc[1].PersonDoNotCall = False;
                update lst_acc;
                
                BIIB_Affiliation__c testAffiliation = BIIB_testClassHelper.createAffiliation(lst_acc[0],lst_acc[1]);
                insert testAffiliation;
                for(Account a : lst_acc){
                    a.FirstName = 'UpdateTest';
                }
                BIIB_Affiliation__c testrec = new BIIB_Affiliation__c();
                testrec = [Select id,BIIB_Affiliation_Type__c, BIIB_From_Account__c, BIIB_Primary__c, BIIB_To_Account__c,BIIB_From_Account_Type__c,BIIB_To_Account_Type__c from BIIB_Affiliation__c where BIIB_From_Account__c = :lst_Acc[1].id];
                System.debug('#@# testrec '+testrec );
                System.debug('#@# testAffiliation '+testAffiliation );
                System.debug('#@# testAffiliation type '+testAffiliation.BIIB_Affiliation_Type__c );
                System.debug('#@# ToAcc type '+testAffiliation.BIIB_To_Account_Type__c );
                System.debug('#@# From Acc type '+testAffiliation.BIIB_From_Account_Type__c);
                update lst_acc;
                
            test.stopTest();

        }
        
}