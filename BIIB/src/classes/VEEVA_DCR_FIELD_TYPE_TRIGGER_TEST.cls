@isTest
private class VEEVA_DCR_FIELD_TYPE_TRIGGER_TEST {

    static testMethod void testInsertProfile() {
        Profile testProfile = [SELECT Id, Name from Profile LIMIT 1];
        DCR_Field_Type_vod__c fieldType = new DCR_Field_Type_vod__c(Object_API_Name_vod__c='Account',
                                                                    Country_vod__c='QA',
                                                                   Record_Type_vod__c='Professional_vod',
                                                                   Field_API_Name_vod__c='Name',
                                                                   Field_Type_vod__c='DCR_vod',
                                                                   Profile_ID_vod__c=testProfile.Id);
        System.Test.startTest();
        insert fieldType;
        System.Test.stopTest();
        DCR_Field_Type_vod__c inserted = [SELECT Id, Profile_Name_vod__c,Unique_Key_vod__c FROM DCR_Field_Type_vod__c
                                          WHERE Id= :fieldType.Id];
        System.assert(inserted != null);
        System.assertEquals(inserted.Profile_Name_vod__c, testProfile.Name);
        System.assertEquals(inserted.Unique_Key_vod__c, 'QA:'+inserted.Profile_Name_vod__c+':Account:Professional_vod:Name');
    }
    
    static testMethod void testInsertNoProfile() {
        DCR_Field_Type_vod__c fieldType = new DCR_Field_Type_vod__c(Object_API_Name_vod__c='Account',
                                                                    Country_vod__c='QA',
                                                                   Record_Type_vod__c='Professional_vod',
                                                                   Field_API_Name_vod__c='Name',
                                                                   Field_Type_vod__c='DCR_vod');
        System.Test.startTest();
        insert fieldType;
        System.Test.stopTest();
        DCR_Field_Type_vod__c inserted = [SELECT Id, Profile_Name_vod__c,Unique_Key_vod__c FROM DCR_Field_Type_vod__c
                                          WHERE Id= :fieldType.Id];
        System.assert(inserted != null);
        System.assert(inserted.Profile_Name_vod__c == null);
        System.assertEquals(inserted.Unique_Key_vod__c, 'QA::Account:Professional_vod:Name');
    }
    
    static testMethod void testInsertNoRecordTypeNoProfile() {
        DCR_Field_Type_vod__c fieldType = new DCR_Field_Type_vod__c(Object_API_Name_vod__c='Account',
                                                                    Country_vod__c='QA',
                                                                   Field_API_Name_vod__c='Name',
                                                                   Field_Type_vod__c='DCR_vod');
        System.Test.startTest();
        insert fieldType;
        System.Test.stopTest();
        DCR_Field_Type_vod__c inserted = [SELECT Id, Profile_Name_vod__c,Unique_Key_vod__c FROM DCR_Field_Type_vod__c
                                          WHERE Id= :fieldType.Id];
        System.assert(inserted != null);
        System.assert(inserted.Profile_Name_vod__c == null);
        System.assertEquals(inserted.Unique_Key_vod__c, 'QA::Account::Name');
    }
    
    static testMethod void testInsertNoRecordType() {
        Profile testProfile = [SELECT Id, Name from Profile LIMIT 1];
        DCR_Field_Type_vod__c fieldType = new DCR_Field_Type_vod__c(Object_API_Name_vod__c='Account',
                                                                    Country_vod__c='QA',
                                                                   Field_API_Name_vod__c='Name',
                                                                   Field_Type_vod__c='DCR_vod',
                                                                   Profile_ID_vod__c=testProfile.Id);
        System.Test.startTest();
        insert fieldType;
        System.Test.stopTest();
        DCR_Field_Type_vod__c inserted = [SELECT Id, Profile_Name_vod__c,Unique_Key_vod__c FROM DCR_Field_Type_vod__c
                                          WHERE Id= :fieldType.Id];
        System.assert(inserted != null);
        System.assertEquals(inserted.Profile_Name_vod__c,testProfile.Name);
        System.assertEquals(inserted.Unique_Key_vod__c, 'QA:'+inserted.Profile_Name_vod__c+':Account::Name');
    }
}