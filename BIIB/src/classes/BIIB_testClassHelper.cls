public class BIIB_testClassHelper{

    public static List<RecordType> acc_rectype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND DeveloperName  in ('BIIB_HCP' , 'BIIB_Patient' , 'BIIB_HCO' , 'BIIB_SPP' , 'BIIB_Infusion_Sites' , 'BIIB_Payer' , 'BIIB_Other_Individuals', 'Professional_vod' )  ORDER BY DeveloperName];
    public static List<RecordType> case_rectype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Case' AND Name  in ('12 Week Questionnaire' , 'Adverse Event' , 'Benefit Investigation' , 'Financial Assistance' , 'Medicare Outreach' , 'Patient Check-In' , 'Support')  ORDER BY Name];
    
    public static Account createAccountHCP(String fname,String lname){
        Account acc_rec = new Account(FirstName = fname , LastName = lname , RecordTypeId = acc_rectype[1].id , PersonDoNotCall = true);
        return acc_rec;
    }
    
    public static Account createAccountPatient(String fname,String lname){
        Account acc_rec = new Account(FirstName = fname , LastName = lname , RecordTypeId = acc_rectype[4].Id , PersonDoNotCall = true);
        return acc_rec;
    }
    
    public static Account createAccountHCO(String accname){
        Account acc_rec = new Account(Name = accname , RecordTypeId = acc_rectype[0].Id);
        return acc_rec;
    }
    public static Account createAccountSPP(String accname){
        Account acc_rec = new Account(Name = accname , RecordTypeId = acc_rectype[6].Id);
        return acc_rec;
    }
    
    public static Account createAccountInfusionSites(String accname){
        Account acc_rec = new Account(Name = accname , RecordTypeId = acc_rectype[2].Id);
        return acc_rec;
    }
    
    public static Account createAccountPayer(String accname){
        Account acc_rec = new Account(Name = accname , RecordTypeId = acc_rectype[5].Id);
        return acc_rec;
    }
    
    public static Account createAccountOtherIndividuals(String fname,String lname){
        Account acc_rec = new Account(FirstName = fname , LastName = lname , RecordTypeId = acc_rectype[3].Id , PersonDoNotCall = true);
        return acc_rec;
    }
    
    public static Account createAccountProfessionalVod(String fname,String lname){
        Account acc_rec = new Account(FirstName = fname , LastName = lname , RecordTypeId = acc_rectype[7].Id , PersonDoNotCall = true);
        return acc_rec;
    }
    
     public static BIIB_Affiliation__c  createAffiliation(Account toAcc, Account fromAcc){
         BIIB_Affiliation__c aff_rec = new BIIB_Affiliation__c(BIIB_Effective_Date__c = date.newInstance(2014, 4, 20) , BIIB_End_Date__c = date.newInstance(2014, 4, 21) , BIIB_To_Account__c = toAcc.id , BIIB_From_Account__c = fromAcc.id , BIIB_Preferred__c = True  , BIIB_Primary__c = True);
         return aff_rec;
    }
    
    public static Case createCase(){
    //RecordType = Medical Outreach
        Account tempAcc = BIIB_testClassHelper.createAccountPatient('testcase','account');
        insert tempAcc;
        Case tempCase = new Case();
            tempCase.AccountId = tempAcc.id;
            tempCase.Status = 'New';
            tempCase.recordTypeId = case_rectype[4].Id;
        return tempCase;
    }
    
    public static BIIB_Work__c createWork(User tempUser, Case tempCase){
    BIIB_Work__c tempWork = new BIIB_Work__c();
            tempWork.Name = 'testWork';
            tempWork.BIIB_Assigned_to__c = tempUser.Id;
            tempWork.BIIB_Status__c = 'In Progress';
            tempWork.BIIB_Preferred_Language__c = 'English';
            tempWork.BIIB_Case__c = tempCase.Id;
    return tempWork;
    }
}