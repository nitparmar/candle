<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BIIB_Uncheck_Preferred_field</fullName>
        <description>US-0171 : mark the Preferred flag as false in case the end date is reached.</description>
        <field>BIIB_Preferred__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Preferred field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Uncheck_Primary_field</fullName>
        <description>US - 0171 : uncheck primary field when end date is reached.</description>
        <field>BIIB_Primary__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Primary field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Primary</fullName>
        <description>US-0171 : Uncheck primary checkbox.</description>
        <field>BIIB_Primary__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Primary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Uncheck Preferred Affiliation on Inactive</fullName>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Affiliation__c.BIIB_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Affiliation__c.BIIB_Preferred__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Affiliation__c.BIIB_Effective_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>US-0171 : Uncheck Preferred field in case the end date is reached.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BIIB_Uncheck_Preferred_field</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>BIIB_Affiliation__c.BIIB_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Uncheck Primary Affiliation on Inactive</fullName>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Affiliation__c.BIIB_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Affiliation__c.BIIB_Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Affiliation__c.BIIB_Effective_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>US-0171 : Uncheck primary field of affiliation after inactivation of affiliation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Uncheck_Primary</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>BIIB_Affiliation__c.BIIB_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
