<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BIIB_Birthdate_Text_Field_Update</fullName>
        <description>User Story US-0141: Field update to capture the PersonBirthdate field value in Birthdate Text field</description>
        <field>BIIB_Birthdate_Text__c</field>
        <formula>TEXT(MONTH(PersonBirthdate)) + &quot;/&quot; + TEXT(DAY(PersonBirthdate)) + &quot;/&quot; + TEXT(YEAR(PersonBirthdate))</formula>
        <name>Birthdate Text Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Dup_Criteria_1_field</fullName>
        <description>User story US-0116. Updates the BIIB_Dup_Criteria field to FirstName + Last Name + Email only for Patient records</description>
        <field>BIIB_Dup_Criteria__c</field>
        <formula>FirstName + &quot;-&quot; + LastName + &quot;-&quot; + PersonEmail</formula>
        <name>Update Dup Criteria 1 field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Dup_Criteria_2_field</fullName>
        <description>User story US-0116. Updates the BIIB_Dup_Criteria_1 field to FirstName + Last Name + Birthdate only for Patient records</description>
        <field>BIIB_Dup_Criteria_2__c</field>
        <formula>FirstName + &quot;-&quot; + LastName + &quot;-&quot; + TEXT(MONTH(PersonBirthdate)) + &quot;/&quot; + TEXT(DAY(PersonBirthdate)) + &quot;/&quot; + TEXT(YEAR(PersonBirthdate))</formula>
        <name>Update Dup Criteria 2 field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BIIB_Update_Birthdate_Text</fullName>
        <actions>
            <name>BIIB_Birthdate_Text_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Story US-0141: Workflow to update the PersonBirthdate field value to Birthdate Text field</description>
        <formula>ISCHANGED( PersonBirthdate )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Update_Dup_Criteria</fullName>
        <actions>
            <name>BIIB_Update_Dup_Criteria_1_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BIIB_Update_Dup_Criteria_2_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient</value>
        </criteriaItems>
        <description>User story US-0116. Updates the BIIB_Dup_Criteria field to FirstName + Last Name + Email and BIIB_Dup_Criteria_2 field to FirstName + Last Name + Birthdate only for Patient records</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.BIIB_Risk_Type__c</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
