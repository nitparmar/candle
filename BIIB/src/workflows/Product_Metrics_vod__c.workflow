<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Product Metric External Id</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Product_Metrics_vod__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This Workflow is designed to set the Product_Metric_vod External_ID_vod field on create/edit.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
