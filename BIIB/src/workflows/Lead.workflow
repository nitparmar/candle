<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notifiaction_to_IAM_Queue_Member</fullName>
        <description>Email notifiaction to IAM Queue Member</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_to_IAM_about_Touch_Participation</template>
    </alerts>
    <rules>
        <fullName>BIIB Email Notification To IAM Queue Members</fullName>
        <actions>
            <name>Email_notifiaction_to_IAM_Queue_Member</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>US-0215: Send an email notification to the IAM queue members when a new Lead record is assigned to the queue based on Interested in TOUCH flag.</description>
        <formula>Owner:Queue.QueueName == &apos;IAM&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
