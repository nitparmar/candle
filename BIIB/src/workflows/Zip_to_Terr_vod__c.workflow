<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ZipIdExternalId_vod</fullName>
        <field>Zip_ID_vod__c</field>
        <formula>Name</formula>
        <name>Zip Id External Id_vod</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Zip to Terr External Id_vod</fullName>
        <actions>
            <name>ZipIdExternalId_vod</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update External Id on Zip to Terr object for data loading.</description>
        <formula>IF(ISNULL(Name), false, true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
