<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BIIB_combination_Field_Update</fullName>
        <description>US-230: This field update is used to populate the combination of Plan Id and Product Id in the BIIB_Combination__c field</description>
        <field>BIIB_Combination__c</field>
        <formula>CASESAFEID(BIIB_Plan__c) + &apos;-&apos; + CASESAFEID( BIIB_Product__c)</formula>
        <name>BIIB_combination_Field_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BIIB_Update_Unique_field_on_Pref_SPP</fullName>
        <actions>
            <name>BIIB_combination_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_PreferredSPPPlan__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>US-230: This worklow is used to populate the combination of Plan Id and Product Id using field update in the BIIB_Combination__c field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
